(ns aoc-2021.day-1)

(defn input-value [path]
  (slurp path))

(def input (memoize input-value))

(def values (input "./resources/aoc_day_1_input.txt"))

(defn- parse [input]
  (->> (clojure.string/split input #"\n")
       (map #(Integer/parseInt %))))

(defn first-member? [value]
  (= value nil))

(defn increased? [old current]
  (> current old))

(defn first-star [input]
  (-> (reduce (fn [acc value]
                (if (first-member? (:old-value acc))
                  (assoc acc :old-value value)
                  (if (increased? (:old-value acc) value)
                    (assoc acc :acc (inc (:acc acc))
                           :old-value value)
                    (assoc acc :old-value value))))
              {:acc 0
               :old-value nil}
              input)
      :acc))

(defn- sum-values [input]
  (let [i (count input)
        last-i (- i 1)]
    (loop [current-i 0
           state []]
      (if (= current-i last-i)
        state
        (recur (inc current-i)
               (conj state (+ (nth input current-i)
                              (nth input (inc current-i) 0)
                              (nth input (inc (inc current-i)) 0))))))))

(defn second-star []
  (-> (parse values)
      sum-values
      first-star))

#_(second-star)

(defn- proccess-state [state old-sum current-sum]
  (if (and (not= 0 old-sum)
           (> current-sum old-sum))
    (inc state)
    state))

(defn sum-values-one-time [input]
  (let [i      (count input)
        last-i (- i 1)]
    (loop [current-i 0
           last-sum  0
           state     0]
      (if (= current-i last-i)
        state
        (recur (inc current-i)
               (+ (nth input current-i)
                  (nth input (inc current-i) 0)
                  (nth input (inc (inc current-i)) 0))
               (proccess-state state last-sum (+ (nth input current-i)
                                                 (nth input (inc current-i) 0)
                                                 (nth input (inc (inc current-i)) 0))))))))

#_(-> (parse values)
      sum-values-one-time)
